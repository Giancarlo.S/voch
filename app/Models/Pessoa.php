<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'idUsuario',
        'Nome',
        'Idade',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
   
    protected $primaryKey = 'idPessoa';

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $table = 'tb_pessoa';
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    public function usuario()
    {
        return $this->hasOne(Phone::class, 'idUsuario');
    }
}
