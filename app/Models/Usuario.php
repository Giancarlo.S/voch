<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Usuario extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'email',
        'senha',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'senha'
    ];
    protected $primaryKey = 'idUsuario';

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $table = 'tb_usuario';
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    
}
