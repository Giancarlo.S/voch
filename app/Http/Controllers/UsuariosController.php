<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use Illuminate\Support\Facades\Hash;

class UsuariosController extends Controller
{
    public function index(){
        $all_user = Usuario::all();
        return view('/pages/usuarios')->with("usuarios", $all_user);
    }

    public function destroy($id){
        Usuario::find($id)->delete();
        return response()->json([
            'message' => 'Usuario deleted successfully!'
          ]);
    }

}
