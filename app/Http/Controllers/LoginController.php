<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function index()
    {
        return view('/login');
    }

    public function check_credencials(Request $request){
        $validated = $request->validate([
            'email' => 'email|required',
            'password' => 'required',
        ]);

        $email = $request->email;
        $form_senha = $request->password;

        //validate_user
        $usuario = Usuario::where('email', $email)->first();
        
        if($usuario === null){
            return redirect()->back()->with('email', 'Email not found');   
        
        }else{
            $senha_usuario = $usuario->senha;
            if (Hash::check($form_senha, $senha_usuario))
            {
                $this->setCookieAuth();
                return redirect("/");
                
            }else{
                return redirect()->back()->with('password', 'Wrong Password');  
            }
        }
    }

    public function signup(){
        return view('/signup');
    }

    public function create_usuario(Request $request){
        $validated = $request->validate([
            'email' => 'email|required|unique:tb_usuario,email',
            'password' => 'required',
        ]);

        $new_user = new Usuario;
        $hash_senha = Hash::make($request->password);
        $new_user->email = $request->email;
        $new_user->senha = $hash_senha;
        $new_user->save();
        $this->setCookieAuth();
        return redirect("/");
    }

    public function setCookieAuth(){
        $test = setcookie("token", Hash::make("password"), time() + (86400 * 30), "/");
        return $test;
    }

    public function logout(){
        setcookie('token', '', time() - 3600, '/');
        return redirect("/login");
    }
}
