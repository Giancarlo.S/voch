<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pessoa;
use App\Models\Usuario;

class PessoasController extends Controller
{
    public function todas_pessoas(){
        $all_people = Pessoa::join('tb_usuario', 'tb_usuario.idUsuario', '=', 'tb_pessoa.idUsuario')->get();
        return view('/pages/todas_pessoas')->with("pessoas", $all_people);
    }

    public function destroy($id){
        Pessoa::find($id)->delete();
        return response()->json([
            'message' => 'Pessoa deleted successfully!'
          ]);
    }

    public function index(){
       
        $usuarios = $this->getNotUsedUsuarios();
        return view('/pages/pessoas')->with("usuarios", $usuarios);
    }

    public function criar(Request $request){
        $validated = $request->validate([
            'idUsuario' => 'numeric |required | exists:tb_usuario,idUsuario',
            'Nome' => 'required',
            'Idade' => 'numeric|required'
        ]);

        $Pessoa = new Pessoa;
        $Pessoa->idUsuario = $request->idUsuario;
        $Pessoa->Nome = $request->Nome;
        $Pessoa->Idade = $request->Idade;
        $Pessoa->save();
        return redirect("/todas_pessoas");
    }
        public function edit($id){
        
        $pessoa_edit = Pessoa::join('tb_usuario', 'tb_usuario.idUsuario', '=', 'tb_pessoa.idUsuario')
                    ->where("idPessoa",$id)->first();
        $usuarios = $this->getNotUsedUsuarios();

        return view('/pages/pessoas')->with("usuarios", $usuarios)
                                     ->with("pessoa_edit", $pessoa_edit);
    }

    public function getNotUsedUsuarios(){
        $all_user = Usuario::all();
        $all_people = Pessoa::all();

        $array_user_id = [];
        $array_people = [];
      
        // removendo os usuarios que ja estão em uso
        foreach($all_user as $user_id){
            $array_user_id[] = $user_id->idUsuario;
        }

        foreach($all_people as $people){
            $array_people[] = $people->idUsuario;
        } 
        $array_user_id = array_diff($array_user_id,$array_people);

        $usuarios = Usuario::whereIn('idUsuario', $array_user_id)->get();
        return $usuarios;
    }

    public function update(Request $request){

        $validated = $request->validate([
            'idUsuario' => 'numeric |required | exists:tb_usuario,idUsuario',
            'Nome' => 'required',
            'Idade' => 'numeric|required'
        ]);
        $pessoa_id = intval($request->route('id'));
        $pessoa = Pessoa::find($pessoa_id);
        $pessoa->idUsuario = $request->idUsuario;
        $pessoa->Nome = $request->Nome;
        $pessoa->Idade = $request->Idade;
        $pessoa->save();
        return redirect("/todas_pessoas?=sucesso");

    }

}
