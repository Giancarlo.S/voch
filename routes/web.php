<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UsuariosController;
use App\Http\Controllers\PessoasController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages/home');
});

Route::get('/login', [LoginController::class, 'index']);
Route::post('/login', [LoginController::class, 'check_credencials']);

Route::get('/signup', [LoginController::class, 'signup']);
Route::post('/signup', [LoginController::class, 'create_usuario']);

Route::get('/logout', [LoginController::class, 'logout']);

Route::get('/usuarios', [UsuariosController::class, 'index']);
Route::delete('/usuarios/{id}', [UsuariosController::class, 'destroy'])->name('usuarios.destroy');


Route::get('/todas_pessoas', [PessoasController::class, 'todas_pessoas']);
Route::get('/pessoas', [PessoasController::class, 'index']);
Route::post('/pessoas', [PessoasController::class, 'criar']);
Route::get('/pessoas/{id}', [PessoasController::class, 'edit']);

Route::post('/update/pessoas/{id}', [PessoasController::class, 'update']);

Route::delete('/pessoas/{id}', [PessoasController::class, 'destroy'])->name('pessoas.destroy');