@extends('layout.body')
@section('content')
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="row">
               <h2> Considerações finais </h2>
            </div>
            <div class="row">
               <p> Se quiser para fazer mais testes, pode rodar o comando: <i>php artisan migrate:refresh --seed </i></p>
               <p>  Porém não tem um usuario padrão, tendo que pegar um email direto no BD e usar a senha: <i> password </i> </p>
            </div>
            <div class="col-md-6">
               <div class="row">
                  <h3> Login </h3>
                  <p> Você pode entrar na parte de singUp e criar um usuario na hora, ele possui algumas verificações: Se o e mail ja está em uso, se é email e se há algum campo em branco </i></p>
                  <p> A senha é criptografada pelo Hash do laravel </p>
                  <p> Eu não consegui fazer a real autenticação do usuario por nenhum middleware, então é criado um cookie com o nome de token </p>
                  <p> Nas paginas <i> Privadas </i> é realizado uma verificação se esse cookie está criado, e então da permissão de abrir a pagina </p>
               </div>
               <div class="row">
                  <h3> Style </h3>
                  <p> Por deixar em aberto, para economizar tempo utilizei o bootstrap para o layout</p>
                  <p> Eu apenas relizei o import pela CDN para agilizar, eu ia separar os arquivos de scrip do style, porém eu perdi a maior parte do tempo tentando fazer a auth e não "sobrou" tempo para ajustar</p>
               </div>
               <div class="row">
                  <h3> Jquery e Ajax </h3>
                  <p> Por não utilizar middleware facilitou a implementação de ajax, para que fique mais fluido o uso na hora de excluir registos</p>
                  <p> Na criação eu optei pelo post tradicional para a verificação de erros de formulario</p>
                  <p> A Tabela foi feita com DataTable para melhor manuseio dos dados</p>
               </div>
               <div class="row">
                  <h3> Jquery e Ajax </h3>
                  <p> Por não utilizar middleware facilitou a implementação de ajax, para que fique mais fluido o uso na hora de excluir registos</p>
                  <p> Na criação eu optei pelo post tradicional para a verificação de erros de formulario</p>
                  <p> A Tabela foi feita com DataTable para melhor manuseio dos dados</p>
               </div>
               <div class="row">
                  <h3> Update usando Post</h3>
                  <p> Por algum motivo, quando utilizava o metodo <i> PUT </i> ele preenchia o header e não batia na rota esperada, então precisei trocar o nome da rota e realizar um post</p>
               </div>
               <div class="row">
                  <h3> Uso único de Usuario por Pessoa</h3>
                  <p> Pela relação que pude entender das tabelas, seria um pra um, então quando é criado uma nova pessoa, é removido da criação da proxima, tendo em vista que o mesmo ja está em uso</p>
               </div>
               <div class="row">
                  <h3> Logout</h3>
                  <p> Apenas remove o cookie e redireciona para o login, ai quando você tenta acessar as paginas bloqueadas, não tem mais acesso <i> Ao menos que volte a pagina, ai não aprendi a bloquear isso ainda</i> </p>
               </div>
            </div>
            <div class="col-md-6">
               <div class="row">
                  <h3>Tabela de Usuarios</h3>
                  <p> Criei uma view para ver os usuarios, porém para cadastrar é apenas atraves do sign UP </p> 
                  <p> E como a criação é feita no login, para editar precisaria de outra view, como o CRUD era para pessoas, deixei assim </p> 
               </div>
               <div class="row">
                  <h3>SweetAlert</h3>
                  <p> Ao realizar a ação de excluir algum registro, é emitido um alerta para a confirmação da ação </p> 
            
               </div>
            </div>
         </div>
      </div>
   </div>
@stop