@extends('layout.body')
@section('content')
 <!--Import jQuery before export.js-->
 <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>


 <!--Data Table-->
 <script type="text/javascript"  src=" https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
 <script type="text/javascript"  src=" https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>

 <!--Export table buttons-->
 <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
 <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js" ></script>
 <script type="text/javascript"  src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>

<!--Export table button CSS-->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="container">
   <div class="text-center"> <h2> Tabela de Usuarios </h2> </div>
   
   <table id="table_id" class="display">
      <thead>
          <tr>
            <th scope="col" class='text-center'>idUsuario</th>
            <th scope="col" class='text-center'>Email</th>
            <th scope="col" class='text-center'>Excluir </th>
          </tr>
      </thead>
      <tbody>
         @foreach($usuarios as $user)
         <tr id="tr{{$user->idUsuario}}" >
            <th scope="row" class='text-center'>{{$user->idUsuario}}</th>
            <th scope="row" class='text-center'>{{$user->email}}</th>
            <th style="cursor: pointer;" class='text-center' scope="row" onclick="excluir({{$user->idUsuario}})"><span class="material-symbols-outlined">close</span> </th>
         </tr>
         @endforeach
      </tbody>
   </table>
</div>

   <script type="text/javascript">
      $(document).ready( function () {
         $('#table_id').DataTable();
      } ); 

		var _token = $('meta[name="_token"]').attr('content');
		$.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
         });

      var table = $('#table_id').DataTable();
      
      function excluir(id){
         var obj = $(this);
        	Swal.fire({
			  title: 'Você tem certeza?',
			  text: "Não será possível reverter!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Sim! Excluir'
			}).then((result) => {
			 	if (result.isConfirmed) {
				  	$.ajax({
	                    url: "{{url('usuarios')}}/" +id,
	                    type: 'delete',
	                    success: function(response){
	                    	Swal.fire(
						      'Excluido!',
						      'Usuario excluido com sucesso.',
						      'success'
						    )
                      $("#tr"+id).remove();
                     },
	            	});
			 	}
			})
        }
	</script>

@stop