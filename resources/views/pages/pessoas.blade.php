@extends('layout.body')
@section('content')
<?php
   if(isset($pessoa_edit)){
        $nome = $pessoa_edit->Nome;
        $idade = $pessoa_edit->Idade ;
        $button_txt = "Atualizar" ;
        $method = "PUT" ;
        $route = "/update/pessoas/".$pessoa_edit->idPessoa; 
    } else{
        $nome = "";
        $idade = "" ;
        $button_txt = "Criar" ;
        $method = "POST" ;
        $route = "/pessoas" ;
   }

?>
<div class="container">
    <form action="{{$route}}" method="POST">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <label>idUsuario</label>
                        <select name="idUsuario" id="idUsuario" class="form-control">
                            
                            @if(isset($pessoa_edit))
                            
                                <option value={{$pessoa_edit->idUsuario}} selected >{{$pessoa_edit->email}}</option>
                            @else
                                <option value=0 selected disabled>Selecione uma Opção</option>
                            @endif

                            @foreach($usuarios as $usuario)
                            <option value="{{$usuario->idUsuario}}">{{$usuario->email}}</option>
                            @endforeach
                        </select>
                        @error("idUsuario")
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-3">
                        <label> Nome </label>
                        <input type="text" class="form-control" placeholder="Nome" id="Nome" name="Nome" value="{{ $nome }}" />
                        @error("Nome")
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-3">
                        <label> Idade </label>
                        <input type="number" class="form-control"  id="Idade"  name="Idade" value="{{ $idade }}" />
                        @error("Idade")
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                </br>
                    <div class="col-md-1">
                        <button class="btn btn-success" type="submit" >
                            {{ $button_txt }}
                        </button>
                    </div>
                    <div class="col-md-1">
                        <a href="/todas_pessoas" class="btn btn-danger" type="submit" >
                            Voltar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@stop