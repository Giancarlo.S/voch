@extends('layout.body')
@section('content')
 <!--Import jQuery before export.js-->
 <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>


 <!--Data Table-->
 <script type="text/javascript"  src=" https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
 <script type="text/javascript"  src=" https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>

 <!--Export table buttons-->
 <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
 <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js" ></script>
 <script type="text/javascript"  src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>

<!--Export table button CSS-->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="container">
   <div class="text-center"> <h2> Tabela de Pessoas </h2> </div>
   
   <table id="table_id" class="display">
      <thead>
          <tr>
            <th scope="col">idPessoa</th>
            <th scope="col">idUsuario</th>
            <th scope="col">email</th>
            <th scope="col">Nome</th>
            <th scope="col">Idade</th>
            <th scope="col">Editar </th>
            <th scope="col">Excluir </th>
          </tr>
      </thead>
      <tbody>
         @foreach($pessoas as $pessoa)
         <tr id="tr{{$pessoa->idPessoa}}" >
            <th scope="row">{{$pessoa->idPessoa}}</th>
            <th scope="row">{{$pessoa->idUsuario}}</th>
            <th scope="row">{{$pessoa->email}}</th>
            <th scope="row">{{$pessoa->Nome}}</th>
            <th scope="row">{{$pessoa->Idade}}</th>
            <th style="cursor: pointer;" class='text-center' scope="row" onclick="editar({{$pessoa->idPessoa}})"><span class="material-symbols-outlined">settings</span> </th>
            <th style="cursor: pointer;" class='text-center' scope="row" onclick="excluir({{$pessoa->idPessoa}})"><span class="material-symbols-outlined">close</span> </th>
         </tr>
         @endforeach
      </tbody>
   </table>
</div>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-4">
                <a href="/pessoas" class="btn btn-success" >Criar pessoa </a>
            </div>
        </div>
    </div>

   <script type="text/javascript">
      $(document).ready( function () {
         $('#table_id').DataTable();
      }); 

		var _token = $('meta[name="_token"]').attr('content');
		$.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
         });

      var table = $('#table_id').DataTable();
      
      function editar(id){
         window.location.replace("http://localhost:8000/pessoas/"+id);
      }
      
      function excluir(id){
         var obj = $(this);
        	Swal.fire({
			  title: 'Você tem certeza?',
			  text: "Não será possível reverter!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Sim! Excluir'
			}).then((result) => {
			 	if (result.isConfirmed) {
				  	$.ajax({
	                    url: "{{url('pessoas')}}/" +id,
	                    type: 'delete',
	                    success: function(response){
	                    	Swal.fire(
						      'Excluido!',
						      'Excluido com sucesso.',
						      'success'
						    )
                      $("#tr"+id).remove();
                     },
	            	});
			 	}
			})
        }

	</script>

@stop