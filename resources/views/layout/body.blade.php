<?php 
    if(!isset($_COOKIE['token'])){
        header("Location: http://localhost:8000/login");
        die();
    };
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="voch" content="Blade">
    <title>Seja Bem Vindo</title>
    <!-- load bootstrap from a cdn -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> 
    
</head>
<body>
<div class="container-fluid">
    @include('layout.header')
   <div id="main" class="row">
        @yield('content')
   </div>
   <footer class="row">
       @include('layout.footer')
   </footer>
</div>
</body>
</html>